const Joi = require("joi");
const Database = require('./Database');
const allowedApps = Database.AllowedCuriers;

/**validace příchozích požadavků na uložení do databáze */
/**validace požadavku na zprávy */
exports.validateMessage = function validateMessage(message, allRequired = true) {
	const schema = Joi.object({
		curier: Joi.string().min(3),
		userID: Joi.string().min(3),
		receiverName: Joi.string().min(3),
		receiverID: Joi.string().min(3),
		date: Joi.date(),
		subject : Joi.string().optional(),
		repeatInDays: Joi.number().optional(),
	});

	return schema.validate({
		curier: message.curier,
		userID: message.userID,
		receiverName: message.receiverName,
		receiverID:message.receiverID,
		date: message.date,
		subject: message.subject,
		repeatInDays:message.repeatInDays,
	},{ presence: (allRequired) ? "required" : "optional" });
}
/**validace požadavků na kuryýra */
exports.validateCurier = function (curier,allRequired = true){
	const schema = Joi.object({
		app: Joi.string().valid(...allowedApps),
		userID: Joi.string().min(3),
		userAppID: Joi.string().min(3),
		userAppPassword: Joi.string().min(3),
	});
	return schema.validate({
		app: curier.app,
		userID: curier.userID,
		userAppID: curier.userAppID,
		userAppPassword: curier.userAppPassword
	},{ presence: (allRequired) ? "required" : "optional" })
}

/**validace požadavků na API GET */
exports.validateGet = function(getData) {
	const schema = Joi.object({
		userID: Joi.string().min(3),
		limit: Joi.number().min(1),
		fromDate: Joi.date(),
		toDate: Joi.date(),
		curier: Joi.string().valid(...allowedApps),
		select: Joi.string(),
		receiverName: Joi.string().min(3)
	});

	return schema.validate({
		userID: getData.userID,
		limit: getData.limit,
		fromDate: getData.fromDate,
		toDate: getData.toDate,
		curier: getData.curier,
		select: getData.select,
		receiverName:getData.receiverName
	});
}

/**validace požadavků na API DELETE */
exports.validateDelete = function (deleteData){
	const schema = Joi.object({
		delete: Joi.boolean(),
	});
	return schema.validate({
		delete: deleteData.delete,
	});
}
