const mongoose = require("mongoose");
/**připojení k databázi */
mongoose
	.connect("mongodb://localhost:27017/birthdaydb", { useNewUrlParser: true })
	.then(() => console.log("Connected to MongoDB!"))
	.catch((error) => console.error("Could not connect to MongoDB... ", error));

/**definování kolekcí v mongoDB */
/**schéma pro kolekci messages */
const messageSchema = new mongoose.Schema({
	curier: mongoose.Schema.Types.ObjectId,
	userID: mongoose.Schema.Types.ObjectId,
	receiverName: String,
	receiverID: String,
	date: Date,
	repeatInDays: {
		type: Number,
		default: 0,
	},
	subject: String,
	text: {
		type: String,
		default: 'Ahoj, přeju všechno nejlepší k narozeninám',
	},
}, { collection: 'messages' });

/**schéma pro kolekci users */
const userSchema = new mongoose.Schema({
	name: String,
	email: {
		type: String,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	dateAdded: {
		type: Date,
		default: Date.now,
	}
}, { collection: 'users' });

/**schéma pro kolekci curiers */
const curierSchema = new mongoose.Schema({
	app: String,
	userID: mongoose.Schema.Types.ObjectId,
	userAppID: String,
	userAppPassword: String,
}, { collection: 'curiers' })

/**exporty všech schémat */
exports.Message = mongoose.model("Message", messageSchema);
exports.User = mongoose.model("User", userSchema);
exports.Curier = mongoose.model("Curier", curierSchema);

/**export aplikací, přes které je program schopen zprávy odesílat */
exports.AllowedCuriers = [
	"email",
	"Jabber"
];