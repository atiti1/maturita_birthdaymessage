const { validateDelete } = require("../Validation");
const database = require("../Database");

const Message = database.Message;
const Curier = database.Curier;
const User = database.User;

/**smaže dokument s daným id a typem (message/curier) */
async function deleteItem(id, typeItem) {
    if (typeItem === 'message') {
        return Message.findByIdAndDelete(id);
    } else if (typeItem === 'curier') {
        return Curier.findByIdAndDelete(id);
    } else if (typeItem === 'user') {
        return User.findByIdAndDelete(id);
    } else {
        return ('špatně zadaný příkaz!');
    }
}
/**smaže všechny zprávy spojené s uživatelem */
async function deleteConnectedMessagesToUser(id) {
    return Message.deleteMany({ userID: id });
}

/**smaže všechny zprávy spojené s kurýrem */
async function deleteConnectedMessagesToCurier(id) {
    return Message.deleteMany({ curier: id })
}

/**smaže všechny kurýry spojené s uživatelem */
async function deleteConnectedCuriers(id) {
    if (id) return Curier.deleteMany({ userID: id });
}

/**validuje body DELETE requestu */
function valDel(req, res) {
    const { err } = validateDelete(req);
    if (err) {
        res.status(400).send(error.details[0].message)
        return;
    }
}

/**poskládá všechny delete  funkce, které mají proběhnout */
function doneDelete(id, typeItem, res) {
    deleteItem(id, typeItem)
        .then((resu) => {
            if (resu) res.send(typeItem + 'byl smazán');
            else res.status(404).send(typeItem + 'se nepodařilo smazat');
        })
        .catch((err) => {
            console.log(err);
            res.send(err.error);

        })
}

/**export funkce která smaže zprávu */
exports.deleteMessage = function (req, res) {
    doneDelete(req.params.id, 'message', res);
}

/**export funkce, která smaže kurýra */
exports.deleteCurier = function (req, res) {
    id = req.params.id;
    Message.find().where('curier', id).countDocuments()
        .then((count) => {
            if (count > 0) {
                valDel(req, res);
                if (req.body.delete) {
                    deleteConnectedMessagesToCurier(id)
                        .then((result) => {
                            if (result) doneDelete(id, 'curier', res);
                            else res.status(404).send('zprávy se nepodařilo smazat');
                        })
                        .catch((err) => res.send(err.details[0].message));
                } else {
                    res.status(400).json({
                        success: false,
                        message:
                            "Opravdu chcete smazat připojení, ke kterému je připojena alepoň jedna zpráva? Budou smazány i zprávy",
                    });
                }
            } else {
                doneDelete(id, 'curier', res);
            }
        })

}

/**export funkce, která smaže uživatele */
exports.deleteUser = function (req, res) {
    id = req.params.id;
    valDel(req, res);
    if (req.body.delete) {
        deleteConnectedMessagesToUser(id)
            .then((result) => {
                if (result) deleteConnectedCuriers(id)
                    .then((resu) => {
                        if (result) doneDelete(id, 'user', res);
                        else res.status(404).send('posly se nepodařilo smazat');
                    })
                    .catch((err) => res.send(err.details[0].message));
                else res.status(404).send('zprávy se nepodařilo smazat');
            })
            .catch((err) => res.send(err.details[0].message));
    } else {
        res.status(400).json({
            success: false,
            message:
                "Opravdu chcete smazat uživatele, ke kterému je připojena alepoň jedna zpráva? Budou smazány i zprávy",
        });
    }
}