const { validateGet } = require("../Validation")
const database = require("../Database");

const Message = database.Message;
const Curier = database.Curier;
const User = database.User;

/**vraátí dokument podle jeho id a typu + u zprávy k ní dopíše i celého kurýra*/
getItemByID = async function (id, itemType) {
	if (itemType === 'curier') item = await Curier.findById(id);
	else if (itemType === 'user') item = await User.findById(id);
	else if (itemType === 'message') item = await Message.findById(id);
	if (item) {
		item = item.toJSON();
		if (itemType === 'message') {
			const curier = await Curier.findById(item.curier).select("app userAppID");
			item.app = curier.toJSON();
		}
		return item;
	}
}

/**export funkce, která najde dokument a odešle ho clientovi */
exports.getItemById = async function (id, itemType, res) {
	getItemByID(id, itemType)
		.then((item) => {
			if (item) { return res.send(item); }
			else return res.status(404).send("Zpráva s daným id nebyla nalezena!");
		})
		.catch((err) => {
			return res.status(400).send('err');
		});
};

/**funkce, která vrací řetězec dokumentů v závislosti na vstupních podmínkách */
exports.getItemsByCathegories = async function (itemType, req, res) {
	userID = req.query.userId;
	const { error } = validateGet(req.query);
	if (error) {
		res.status(404).send(error.details[0].message);
		return;
	}
	if (itemType === 'message') dbQuery = Message.find().where("userID", userID);
	else if (itemType === 'curier') dbQuery = Curier.find().where("userID", userID);
	else return 'Nesprávný itemType';

	if (req.query.select) dbQuery = dbQuery.select(req.query.select);

	if (req.query.receiverName)
		dbQuery = dbQuery.where("receiverName", req.query.receiverName);

	if (req.query.app) {
		var curierID = await Curier.find().where('userID', userID).where('app', req.query.app)
			.then((curiers) => {
				console.log(curiers);
				var idCurier = curiers[0]._id.toString();
				return idCurier
			})
			.catch((err) => {
				console.log(err);
			})
		console.log("idCurier je", curierID);
		dbQuery = dbQuery.where('curier', curierID);

	}

	if (req.query.fromDate)
		dbQuery = dbQuery.where("date").gt(req.query.fromDate);

	if (req.query.toDate) dbQuery = dbQuery.where("date").lt(req.query.toDate);

	if (req.query.limit)
		dbQuery = dbQuery.limit(parseInt(req.query.limit));
	dbQuery
		.then((mess) => {
			console.log(mess);
			res.json(mess);

		})
		.catch((err) => {
			res.status(400).send("Požadavek na zprávy selhal!");
		});
};

/**funkce, která vrací  */
exports.getUsersApps = async function (req, res) {
	userID = req.query.userId;
	console.log("Jsem v getUsersApp", userID, req.params)
	Curier.find().where('userID', userID)
		.then((curiers) => { return res.send(curiers) })
		.catch((err) => {
			return res.status(400).send(err)
		})
}

/** vrací uživatele - jestli existuje a jaká jsou jeho data */
exports.getUser = function (req, res) {
	User.find({ email: req.query.email, password: req.query.password })
		.then((user) => {
			if (user.length == 0) {
				let data = {
					message: 'Zadané heslo a email neodpovídají žádému registrovanému uživateli!',
					data: [],
				};
				res.send(data);
			} else {
				let data = {
					message: 'User Available!',
					data: user,
				};
				console.log(data);
				res.json(data);
			}
		})
		.catch((e) => {
			console.log(e);
			res.status(400).send(e);
		});
}
