const { validateMessage, validateCurier, validateUser } = require("../Validation");
const database = require("../Database");

const Message = database.Message;
const Curier = database.Curier;
const User = database.User;

/**upraví dokumet v závislosti na jeho valstnostech a typu */
async function updateItem(req, typeItem) {
    if (typeItem === 'message') {
        return Message.findByIdAndUpdate(req.params.id, req.body, { new: true });
    } else if (typeItem === 'curier') {
        return Curier.findByIdAndUpdate(req.params.id, req.body, { new: true });
    } else if (typeItem === 'user') {
        return User.findByIdAndUpdate(req.params.id, req.body, { new: true });
    } else {
        res.status(404).send('špatně zadaný příkaz!');
    }

}

/**rozhodne, které typ kolekce to je a následně odešle 
 * odpověď s chybou/úspšnost
 */
exports.putItem = async function (req, typeItem, res) {
    var err;
    if (typeItem === 'message') {
        err = validateMessage.error;
    } else if (typeItem === 'curier') {
        err = validateCurier.error;
    } else if (typeItem === 'user') {
        err = validateUser.error;
    } else {
        res.status(404).send('špatně zadaný body!');
    }
    if (err) {
        res.status(400).send(err);
    } else {
        updateItem(req, typeItem)
            .then((result) => {
                console.log(result);
                res.json(result);
            })
            .catch((err) => {
                console.log(err);
                res.status(404).send("Nepodařilo se změnit " + typeItem + "!", err);
            })
    }
}
