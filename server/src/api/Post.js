const { validateMessage, validateCurier, validateUser } = require("../Validation");
const database = require("../Database");

const Message = database.Message;
const Curier = database.Curier;
const User = database.User;

/**vytvoří dokument v závislosti na jeho valstnostech a typu */
async function createItem(req, typeItem) {
  if (typeItem === 'message') {
    console.log("create.body", req.body);
    return Message.create(req.body)
  } else if (typeItem === 'curier') {
    return Curier.create(req.body)
  } else if (typeItem === 'user') {
    return User.create(req.body)
  } else {
    res.status(404).send('špatně zadaný příkaz!');
  }

}

exports.postItem = function (req, typeItem, res) {
  var error = null;
  if (typeItem === 'message') {
    error = validateMessage(req.body).error;
  } else if (typeItem === 'curier') {
    error = validateCurier(req.body).error;
  } else if (typeItem === 'user') {
    error = validateUser(req.body).error;
  } else {
    res.status(404).send('špatně zadaný příkaz!');
  }

  if (error) {
    res.status(404).send(error.details[0].message);
  } else {
    createItem(req, typeItem)
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        console.log(err);
        res.status(404).send("Nepodařilo se uožit " + typeItem + "!");
      })
  }
}
exports.postUser = function (req, res) {
  User.find({ email: req.body.email })
    .then((user) => {
      if (user.length == 0) {
        User.create(req.body)
          .then((result) => {
            let data = {
              message: 'User Created Successfully!',
              data: result,
            };
            res.json(data);
          })
          .catch((err) => {
            res.json(error);
          });
      } else {
        let data = {
          message: 'User Already Exists!',
        };
        res.send(data);
      }
    })
    .catch((e) => {
      console.log(e);
      res.status(400).send(e);
    });
}