import React from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import MessageDetail from './messages/MessageDetail';
import AppDetail from './apps/AppDetail';
import { useEffect, useState } from "react";
import {
    BrowserRouter as Router,
    Link,
    Redirect,
    Route,
    Switch,
    useHistory
} from "react-router-dom";

import MessageIndex from './messages/MessageIndex'
import AppIndex from './apps/AppIndex'

import MessageForm from "./messages/MessageForm";
import AppForm from "./apps/AppForm";

import LoginPage from './auth/Login';
import RegisterPage from './auth/Register';

import Description from "./Description";
import { ApiDelete } from "./common/Api";
import FlashMessage from "./common/FlalshMessage";

/**základní komponenta celé stránky - obsahuje menu, volá appTable a messageTable */
function App() {
    const history = useHistory();
    const [loggedIn, setLoggedIn] = useState(false);
    const [userIdState, setUserId] = useState('');
    const [open, setOpen] = useState(false);

    /**odhlásí uživatele */
    const logOut = () => {
        localStorage.setItem("is-login", "false");
        localStorage.removeItem("userID");
        const islogin = localStorage.removeItem("is-login");
        setLoggedIn(islogin);
    };

    /**načte viditelné komponenty stránky v závislosti,
     *  zda je uživatel přihlášený (/messages),
     * nebo ne (/login) */

    const handleClose = () => {
        setOpen(false);
    }
    const handleOpen = () => {
        setOpen(true);
    }
    const deleteUser = () => {
        ApiDelete("/api/users/" + localStorage.getItem("userID"), { delete: true })
            .then(logOut())
            .catch((err) => {
                console.log(err)
            })
    }
    /**mění hodnoty v závislosti na změnách stavu stránky (přihlášený/nepřihlášený uživatel apod.) */
    useEffect(() => {
        const loadId = () => {
            const islogin = localStorage.getItem("is-login");
            const userID = localStorage.getItem("userID");
            setUserId(userID);
            setLoggedIn(islogin);
            if (islogin === true) {
                history.push("/messages/" + userIdState);
                <Redirect to={"/messages/" + userIdState} />;
            } else {
                history.push("/login");
                <Redirect to="/login" />;
            }
        };
        loadId();
        if (loggedIn) {
            history.push("/messages/" + userIdState);
            <Redirect to={"/messages/" + userIdState} />;
        } else {
            history.push("/login");
            <Redirect to="/login" />;
        }
        if (localStorage.getItem("is-login") === false) {
            logOut();
        }
    }, [loggedIn, history, userIdState]);
    return (
        /**tvoří menu - nepřihlášený uživatel se dostane na login/register
         * přihlášený uživatel na messages/apps/logout
         */
        <Router>
            <div className="container">
                <nav className="navbar ms-auto navbar-expand-lg navbar-light bg-light">
                    {!loggedIn || loggedIn === null ? (
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item ps-2">
                                <Link to={"/login"} className="nav-link">
                                    Přihlásit se
                                </Link>
                            </li>
                            <li className="nav-item ps-2">
                                <Link to={"/register"} className="nav-link">
                                    Registrovat se
                                </Link>
                            </li>
                            <li className="nav-item ps-2">
                                <Link to={"/description"} className="nav-link">
                                    Jak to funguje
                                </Link>
                            </li>
                        </ul>
                    ) : (
                        <>
                            <ul className="navbar-nav mc-auto ">
                                <li className="nav-item ps-2">
                                    <Link to={"/messages/" + userIdState} className="nav-link">
                                        Zprávy
                                    </Link>
                                </li>
                                <li className="nav-item ps-2">
                                    <Link to={"/apps/" + userIdState} className="nav-link">
                                        Aplikace
                                    </Link>
                                </li>
                                <li className="nav-item ps-2">
                                    <Link to={"/description"} className="nav-link">
                                        Jak to funguje
                                    </Link>
                                </li>
                                <li className="nav-item ps-2 " onClick={logOut} visible={false}>
                                    <Link to={"#"} className="nav-link">
                                        Odhlásit se
                                    </Link>
                                </li>


                            </ul>
                            <ul className="navbar-nav ms-auto col-12 col-md-2 ">
                                <li className="nav-item bg-warning me-auto" onClick={handleOpen}>
                                    <Link to={"#"} className="nav-link btm-warning p-2 ">
                                        Smazat uživatele
                                    </Link>
                                </li>
                            </ul>
                        </>
                    )}
                </nav>
                {open ? <FlashMessage
                    theme="warning"
                    text={<div mb-2>
                        <div className="mb-2">Chcete opravdu smazat uživatele? Spolu s ním budou smazány i všechny zprávy a uložené apliakce.</div>
                        <div >
                            <modal>

                                <button onClick={deleteUser} className="btn btn-sm btn-danger me-2">Ano</button>
                                <button onClick={handleClose} className="btn btn-sm ms-2 btn-primary">Ne</button>

                            </modal>

                        </div>

                    </div>}
                /> : ""}

                {/**nastavuje, kam vede jaký Link */}
                <Switch>
                    {!loggedIn || loggedIn === null ? (
                        <>
                            <Route path="/login" component={LoginPage} />
                            <Route path="/register" component={RegisterPage} />
                            <Route path="/description" component={Description} />
                            <Redirect exact from="/" to="/login" />
                        </>
                    ) : (
                        <>
                            <Route exact path="/messages/:userId" component={MessageIndex} />
                            <Route path="/messages/show/:id" component={MessageDetail} />
                            <Route path="/messages/create/" component={MessageForm} />
                            <Route path="/messages/edit/:id" component={MessageForm} />

                            <Route exact path="/apps/:userId" component={AppIndex} />
                            <Route path="/apps/show/:id" component={AppDetail} />
                            <Route path="/apps/create/" component={AppForm} />
                            <Route path="/apps/edit/:id" component={AppForm} />

                            <Route path="/description" component={Description} />
                            <Redirect exact from="/" to={"/messages/" + localStorage.userID} />

                        </>
                    )}
                </Switch>
            </div>

        </Router>
    );
}
export default App;
