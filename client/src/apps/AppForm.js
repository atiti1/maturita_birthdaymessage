import React, { useState, useEffect } from 'react';
import FlashMessage from '../common/FlalshMessage';
import { ApiGet } from '../common/Api';
import { InputField } from '../common/formFields/InputField';
import { InputSelect } from '../common/formFields/InputSelect';
import { useHistory } from 'react-router-dom';
import { ApiPut, ApiPost } from '../common/Api';

/**vrací formulář pro úpravu stávající, nebo vytvoření nové aplikace, jako argument bere props, se kterými 
 * je zavolána - využívá z nich pak props.match.params.id */
const AppForm = (props) => {
    const [appIdState, setAppId] = useState(null);
    const [appState, setApp] = useState('email');
    const [userAppIDState, setUserAppID] = useState('');
    const [userAppPasswordState, setUserAppPassword] = useState('');
    const [allowedAppsState, setAllowedApps] = useState([]);
    const [userIDLabelState, setUserIDLabel] = useState("")
    const [sentState, setSent] = useState(false);
    const [successState, setSuccess] = useState(false);
    const [errorState, setError] = useState()
    const history = useHistory();
    const userIDState = localStorage.getItem("userID");

    /**při změně stavu ve formuláři nastaví formulářové hodnoty na nové*/
    const handleChange = (e) => {
        const target = e.target;

        const temp = target.value;

        const name = target.name;
        const value = temp;

        if (name === 'app') { setApp(value); }
        else if (name === 'userAppID') { setUserAppID(value); }
        else if (name === 'userAppPassword') { setUserAppPassword(value); }
    };

    /**odešle hodnoty zadané ve formuláři na serverovou API (buď post, nebo put - podle toho, jestli byla
     * celá komponenta zavolána   bez nebo s id zprávy) */
    const handleSubmit = (e) => {
        e.preventDefault();
        const body = {
            app: appState,
            userID: userIDState,
            userAppID: userAppIDState,
            userAppPassword: userAppPasswordState
        };
        (appIdState
            ? ApiPut('/api/curiers/' + props.match.params.id, body)
            : ApiPost('/api/curiers/', body)
        )
            .then((data) => {
                setSent(true);
                setSuccess(true);
                history.push('/apps/' + userIDState)

            })
            .catch(async (error) => {
                const er = await error;
                setError(er.message);
                setSent(true);
                setSuccess(false);
            });
    };
    /**při načtení záznamu nastaví počáteční hodnoty na hodnoty získané z databáze */
    useEffect(() => {
        const id = props.match.params.id || null;

        if (id) {
            setAppId(id);

            ApiGet("/api/curiers/" + id).then((data) => {
                setApp(data.app);
                setUserAppID(data.userAppID);
                setUserAppPassword(data.userAppPassword);
            });
        }
        ApiGet('/api/allowedApps').then((data) => setAllowedApps(data));
        if (appState === "messenger") {
            setUserIDLabel("Zadejte svoje id na messengeru");
        } else if (appState === "email") {
            setUserIDLabel("Zadejte svůj email")
        }
    }, [props.match.params.id]);

    const sent = sentState;
    const success = successState;
    /**vykreslení samotného formuláře */
    return (
        <div>
            <h1>{props.match.params.id ? "Upravit" : "Vytvořit"} aplikaci</h1>
            <hr />
            {errorState ? <div className="alert alert-danger">{errorState}</div> : ""}
            {sent && (
                <FlashMessage
                    theme={success ? "success" : ""}
                    text={success ? "Uložení přístupu k aplikaci proběhlo úspěšně." : ""}
                />
            )}

            <form onSubmit={handleSubmit}>
                <InputSelect
                    required={true}
                    name="app"
                    items={allowedAppsState}
                    label="aplikace"
                    prompt="Vyberte aplikaci"
                    value={appState}
                    handleChange={handleChange}
                />

                <InputField
                    required={true}
                    type="text"
                    name="userAppID"
                    label="účet uživatele"
                    prompt={userIDLabelState}
                    min="0"
                    value={userAppIDState}
                    handleChange={handleChange}
                />

                <InputField
                    required={true}
                    type="password"
                    name="userAppPassword"
                    label="heslo k profilu uživatele"
                    prompt="zadejte svoje heslo: "
                    min="0"
                    value={userAppPasswordState}
                    handleChange={handleChange}
                />

                <input type="submit" className="btn btn-primary" value="Uložit" />
            </form>
        </div>
    );
}

export default AppForm;