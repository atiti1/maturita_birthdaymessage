import React from "react";
import { Link } from 'react-router-dom';
import { ApiDelete } from '../common/Api';

/**vypíše tabulku náhledu jednotlivých aplikací uživatele včetně tlačítek na jejich úpravu, smazání apod. */
const AppTable = (props) => {
    const deletee = (id) => {
        ApiDelete("/api/curiers/" + id, { delete: true }).then((data) => { props.delete(); window.location.href = "/messages" });
    };
    return (
        <div>
            <p>
                {props.label} {props.items.length}
            </p>
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>aplikace</th>
                        <th colSpan={3}>akce</th>
                    </tr>
                </thead>
                <tbody>
                    {props.items.map((item, index) => (
                        <tr key={index + 1}>
                            <td>{index + 1}</td>
                            <td><b>{item.app}</b> {" (" + item.userAppID + ")"}</td>
                            <td>
                                <div className="btn-group">
                                    <Link to={"/apps/show/" + item._id} className="btn btn-sm btn-info">
                                        Zobrazit
                                    </Link>
                                    <Link to={'/apps/edit/' + item._id} className="btn btn-sm btn-warning">
                                        Upravit
                                    </Link>
                                    <button
                                        onClick={deletee.bind(this, item._id)}
                                        className="btn btn-sm btn-danger"
                                    >Odstranit</button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <div>
                <Link to={'/apps/create/'} className="btn btn-sm btn-warning">
                    Přidat aplikaci
                </Link>
            </div>
        </div>
    )
}

export default AppTable;