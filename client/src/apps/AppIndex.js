import { ApiGet } from "../common/Api";
import { useEffect, useState } from "react";
import AppTable from "./AppTable"

/**komponent pro vykreslení aplikací, dále volá AppTable.js */
const AppIndex = (props) => {
    const [appState, setAppState] = useState([]);
    useEffect(() => {
        const params = { userId: props.match.params.userId };
        ApiGet("/api/curiers/", null, params).then((data) => { setAppState(data); })
    }, [props.match.params.userId]);
    return (
        <div>
            <h1>Seznam Aplikací</h1>
            <AppTable items={appState} label="Počet aplikací:" />
        </div>
    )
}

export default AppIndex;