import React, { useState, useEffect } from "react";
import { ApiGet } from "../common/Api";
import PasswordStringFormatter from "../common/PasswordStringFormatter";
import { Link } from "react-router-dom";

/**načte detail každé aplikce + tlačítka pro možnost upravit a vrátit se zpět*/
const AppDetail = (props) => {
  const [appState, setAppState] = useState("");
  const [userAppIDState, setUserAppID] = useState("");
  const [userAppPassword, setUserAppPassword] = useState("");

  /**při načtení záznamu načte počáteční hodnoty */
  useEffect(() => {
    ApiGet("/api/curiers/" + props.match.params.id)
      .then((data) => {
        setAppState(data.app);
        setUserAppID(data.userAppID);
        setUserAppPassword(data.userAppPassword);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [props.match.params.id]);

  var userIDdescription = "email odesílatele:"
  if (appState === "messenger") {
    userIDdescription = "uživatelské jméno odesílatele:"
  }
  /**vykreselní samotného formuláře */
  return (
    <div>
      <div>
        <h1>Detail kurýra:</h1>
        <hr />
        <p>
          <strong>aplikace: </strong>{appState}
          <br />
          <strong>{userIDdescription} </strong>
          {userAppIDState}
          <br />
          <strong>heslo: </strong>
          {PasswordStringFormatter(userAppPassword)}
        </p>
      </div>
      <div className="btn-group">
        <div>
          <Link to={'/apps/' + localStorage.userID} className="btn btn-sm btn-primary ">
            Zpět
          </Link>
        </div>

        <div>
          <Link to={'/apps/edit/' + props.match.params.id} className="btn btn-sm btn-warning ">
            Upravit
          </Link>
        </div>

      </div>
    </div>
  );
};

export default AppDetail;
