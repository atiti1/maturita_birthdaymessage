import React from "react";

/**komponenta pro vykreslení inputSelect */
export function InputSelect(props) {
  const multiple = props.multiple;
  const required = props.required || false;


  /** příznak označení prázdné hodnoty */
  const emptySelected = multiple ? props.value.length === 0 : !props.value;
  /** příznak objektové struktury položek */
  const objectItems = props.enum ? false : true;

  /**vrací inputSelect s vlastnostmi */
  return (
    <div className="form-group">
      <label>{props.label}:</label>
      <select
        required={required}
        className="browser-default form-select"
        multiple={multiple}
        name={props.name}
        onChange={props.handleChange}
        value={props.value}
      >
        {required ? (
          <option disabled value={emptySelected}>
            {props.prompt}
          </option>
        ) : (
          <option key={0} value={emptySelected}>
            ({props.prompt})
          </option>
        )}

        {objectItems
          ? /* vykreslení položek jako objektů z databáze */
          props.items.map((item, index) => (
            <option
              key={required ? index : index + 1}
              value={item}
            >
              {item}
            </option>
          ))
          : /* vykreslení položek jako hodnot z výčtu  */
          props.items.map((item, index) => (
            <option
              key={required ? index : index + 1}
              value={item}
            >
              {props.enum[item]}
            </option>
          ))}
      </select>
    </div>
  );
}

export default InputSelect;