/**formátuje datum - první argument je datum, druhý argument je
 * jestli to má být v české "uživatelské verzi", nebo v nodejs verzi
 * třetí argument je jestli má být měsíc jako číslo, nebo jako název měsíce
 */
export const DateStringFormatter = (str, locale = false, type = "long") => {
  const d = new Date(str);

  if (locale) {
    return d.toLocaleDateString("cs-CZ", {
      year: "numeric",
      month: type,
      day: "numeric",
    });
  }

  const year = d.getFullYear();
  const month = "" + (d.getMonth() + 1);
  const day = "" + d.getDate();

  return [
    year,
    month.length < 2 ? "0" + month : month,
    day.length < 2 ? "0" + day : day,
  ].join("-");
};

export default DateStringFormatter;