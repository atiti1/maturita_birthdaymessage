import React from "react";

/**vykreslí alert uživateli - jako parametry bere text a téma */
export function FlashMessage(props) {
  const theme = props.theme;
  const text = props.text;

  return <div className={"alert alert-" + theme}>{text}</div>;
}

export default FlashMessage;