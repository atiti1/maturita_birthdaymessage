
import React, { useState } from "react";




const Collapsible = ({ open, children, title }) => {
  const [isOpen, setIsOpen] = useState(open);

  const handleFilterOpening = () => {
    setIsOpen((prev) => !prev);
  };

  return (
    <>
      <div className="card border-0 rounded-0">
        <div>
          <div className="p-0 ml-0 mr-0 d-flex justify-content-between">
            <nav type="button" className="nav" onClick={handleFilterOpening}>
              {!isOpen ? (
                <h3 className="font-weight-bold">{title}</h3>

              ) : (
                <h3 className="font-weight-bold">{title}</h3>
              )}
            </nav>
          </div>
        </div>

        <div className="">
          <div>{isOpen && <div className="p-3">{children}</div>}</div>
        </div>
      </div>
    </>
  );
};

export default Collapsible;

