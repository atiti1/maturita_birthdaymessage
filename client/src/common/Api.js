import axios from "axios";

const API_URL = "http://localhost:5000";

/**volá API GET (parametry: zbytek url, header, params) */
export const ApiGet = (url, head, par) => {
    return axios({ 
        method: "get",
        url: API_URL + url, 
        params: par})
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            throw error;
        });
};

/**volá API POST (parametry: zbytek url, body) */
export const ApiPost = (url, body) =>{
    return axios({
        method: "post",
        url: API_URL + url,
        data: body
    }).then((response) =>{
        return response.data;
    })
    .catch((err) =>{
        throw err;
    });
};

/**volá API PUT (parametry: zbytek url, body) */
export const ApiPut = (url, body) =>{
    return axios({
        method: "put",
        url: API_URL + url,
        data: body
    }).then((response) =>{
        return response.data;
    })
    .catch((err) =>{
        throw err;
    });
};

/**volá API GET (parametry: zbytek url, data () */
export const ApiDelete = (url,data = null) =>{
    return axios({
        method: "delete",
        url: API_URL + url,
        data: data
    }).then((response) =>{
        return response.data;
    })
    .catch((err) =>{
        throw err;
    });
};