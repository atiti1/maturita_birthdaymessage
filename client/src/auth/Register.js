import React, { useState } from 'react';
import { ApiPost } from '../common/Api';
import { Link, } from 'react-router-dom';
import FlashMessage from '../common/FlalshMessage';

/**vypíše formulář pro zadání registračních údajů uživatele a 
 * odešle požadavek na vytvoření nového uživatele
 */
const RegisterPage = (props) => {
    const [emailState, setEmailState] = useState('');
    const [passState, setPassState] = useState('');
    const [cpassState, setCpassState] = useState('');
    const [errorState, setError] = useState('');
    const [registeredState, setRegistered] = useState(false);
    /**při každé změně údajů nastaví nové hodnoty */
    function handleChange(event) {
        if (event.target.name === 'email') {
          setEmailState(event.target.value);
        } else if (event.target.name === 'password') {
          setPassState(event.target.value);
        } else if (event.target.name === 'cpassword') {
          setCpassState(event.target.value);
        } else {
          setEmailState('');
          setPassState('');
        }
     }
     /**odešle požadavek na vytvoření nového uživatele */
     const handleSubmit = (e) => {
        e.preventDefault();
        if (passState !== cpassState) {
          setError("Hesla se musí shodovat!")
        }
        const body = {
            email: emailState,
            password: passState
        }
        ApiPost("/api/addUsers", body)
        .then((res) =>{
          setRegistered(true);
        });
      };
        /**vrací formulář na vyplnění registračních údajů uživatelem
         * (e-mail, 2 x heslo na ověření a odkaz na login )
         */
        return (
            <div className="offset-4 col-sm-6 mt-5">
              {errorState? <FlashMessage
                    theme={"danger"}
                    text={errorState}
                /> :
                ""
                }
                {
                  registeredState?<FlashMessage
                  theme={"success"}
                  text={<div>
                    Registrace proběhla úspěšně. Přihlaš se <Link className="pull-right" to ="/login">
                      zde
                  </Link>
                  !
              </div>}
              /> :
              <FlashMessage
                    theme={"info"}
                    text ={<div>
                    <Link className="pull-right" to ="/login">
                        Už jsi se zaregistroval? Přihlaš se!
                    </Link>
                </div>}
              />
                }
                
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                <label htmlFor="email">E-mail</label>
                <input
                    required={true}
                    type="email"
                    className="form-control"
                    placeholder="E-mail"
                    name="email"
                    value={emailState}
                    onChange={handleChange}
                />
                </div>
                <div className="form-group">
                <label htmlFor="password">Heslo</label>
                <input
                    required={true}
                    type="password"
                    className="form-control"
                    placeholder="Heslo"
                    name="password"
                    value={passState}
                    onChange={handleChange}
                />
                </div>
                <div className="form-group">
                <label htmlFor="cpassword">Heslo znovu</label>
                <input
                    required={true}
                 
                    type="password"
                    className="form-control"
                    placeholder="Heslo"
                    name="cpassword"
                    value={cpassState}
                    onChange={handleChange}
                />
                </div>
                <input type="submit" className="btn btn-danger mt-2" value="Registrovat se" />
            </form>
            </div>
        );
        };


export default RegisterPage;