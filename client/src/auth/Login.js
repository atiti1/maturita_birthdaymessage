import React, { useEffect, useState } from "react";
import { Redirect, useHistory, Link } from "react-router-dom";
import { ApiGet } from "../common/Api";
import FlashMessage from "../common/FlalshMessage";
/**vypíše formulář pro zadání přihlašovacích údajů uživatele a 
 * provede autententizaci uživatele
 */
const LoginPage = (props) => {
  const history = useHistory();
  const [emailState, setEmailState] = useState("");
  const [passState, setPassState] = useState("");
  const [error, setError] = useState("");

  useEffect(() => {
    setError("")
  }, [emailState, passState])
  /**sestaví požadevek na autentizaci uživatele 
   * a odešle požadavek na API 
   * pak do localStorage uloží id uživatele
   * přes history pak udrží uživatele přihlášeného */
  const handleSubmit = (e) => {
    e.preventDefault();
    if (passState === "" || emailState === "") {
      setError("Musíte vyplnit všechna pole!");
    }
    const params = {
      email: emailState,
      password: passState,
    }
    ApiGet("/api/getUser", null, params)
      .then(function (response) {
        const data = response.data[0];
        if (data === undefined) {
          setError(response.message);
        } else {
          localStorage.setItem("is-login", true);
          localStorage.setItem("userID", response.data[0]._id);
          localStorage.setItem("userNick", response.data[0].email);
          const islogin = localStorage.getItem("is-login");
          if (islogin) {
            window.location.href = "/messages";
          } else {
            history.push("/login");
            <Redirect to="/login" />;
          }
        }
      })
      .catch(function (error) {
        setError(error);
      })
      .then(function () {
        // always executed
      });
  };
  /**při každé změně nastaví nové hodnoty */
  function handleChange(event) {
    if (event.target.name === "email") {
      setEmailState(event.target.value);
    } else if (event.target.name === "password") {
      setPassState(event.target.value);
    } else {
      setEmailState("");
      setPassState("");
    }
  }
  /**vrací formulář na zadání údajů uživatelem 
   * (e-mail, heslo + odkaz na registraci)
  */
  return (
    <div className="offset-4 col-sm-6 mt-5">
      {error ?
        <FlashMessage
          theme={"danger"}
          text={error}
        /> :
        ""
      }
      <div className="alert alert-info">

        <Link className="pull-right" to="/register">
          Nemáš ještě účet? Registruj se!
        </Link>
      </div>
      <form onSubmit={handleSubmit}>
        <div className="form-group">
          <label htmlFor="email">E-mail</label>
          <input
            required={true}
            type="email"
            className="form-control"
            placeholder="E-mail"
            name="email"
            value={emailState}
            onChange={handleChange}
          />
        </div>
        <div className="form-group">
          <label htmlFor="password">Heslo</label>
          <input
            required={true}
            type="password"
            className="form-control"
            placeholder="Heslo"
            name="password"
            value={passState}
            onChange={handleChange}
          />
        </div>
        <input type="submit" className="btn btn-success mt-2" value="Přihlásit se" />
      </form>
    </div>
  );
};

export default LoginPage;