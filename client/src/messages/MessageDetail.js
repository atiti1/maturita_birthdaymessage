import React, { useEffect, useState } from "react";
import { ApiGet } from "../common/Api";
import DateStringFormatter from "../common/DateStringFormatter";
import { Link } from "react-router-dom";

/**zobrazí detail zprávy */
const MessageDetail = (props) => {
  const [messageTextState, setMessageText] = useState("");
  const [receiverNameState, setReceiverName] = useState(0);
  const [receiverIDState, setReceiverIDState] = useState("");
  const [dateState, setDate] = useState("");
  const [appState, setAppState] = useState("");
  const [userAppIdState, setUserAppId] = useState("");
  const [messageSubjectState, setMessageSubject] = useState("");
  const [repeatInDaysState, setRepeatInDays] = useState("");
  /**při načtení stránky zavolá API a nastaví počáteční hodnoty zprávy */
  useEffect(() => {
    ApiGet("/api/messages/" + props.match.params.id)
      .then((data) => {
        setMessageText(data.text);
        setReceiverName(data.receiverName);
        setReceiverIDState(data.receiverID);
        setDate(DateStringFormatter(data.date, true));
        setAppState(data.app.app);
        setUserAppId(data.app.userAppID);
        if (data.subject) setMessageSubject(data.subject);
        if (data.repeatInDays) setRepeatInDays(data.repeatInDays);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [props.match.params.id]);


  var receiverIDdescribtion = "email příjemce:"
  if (appState === "messenger") {
    receiverIDdescribtion = "id příjemce na messengeru:"
  }

  /**vrací seznam atributů zprávy */
  return (
    <div>
      <div>
        <h1>Detail zprávy:</h1>
        <hr />

        <p>
          <strong>jméno příjemce:</strong> {receiverNameState}
          <br />
          <strong>{receiverIDdescribtion} </strong>
          {receiverIDState}
          <br />
          <strong>Datum odeslání: </strong>
          {dateState}
          <br />
          <strong>přes aplikaci: </strong>
          {appState}  <span className="text-secondary">{" (" + userAppIdState + ")"}</span>
          <br />
          {messageSubjectState ? <span><strong>přemět: </strong> {messageSubjectState}</span> : ""}
          <br />
          <strong>text zprávy: </strong>
          {messageTextState}
          <br />
          {repeatInDaysState ? <span><strong>opakovat: </strong>každých {repeatInDaysState} dní</span> : ""}
        </p>
      </div>
      <div className="btn-group">
        <Link to={'/messages/' + localStorage.userID} className="btn btn-sm btn-primary ">
          Zpět
        </Link>
        <Link to={'/messages/edit/' + props.match.params.id} className="btn btn-sm btn-warning">
          Upravit
        </Link>
      </div>
    </div>


  );
};

export default MessageDetail;