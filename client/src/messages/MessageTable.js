import React from 'react';
import { Link } from 'react-router-dom';
import { ApiDelete } from '../common/Api';
import DateStringFormatter from '../common/DateStringFormatter';

const MessageTable = (props) => {
    const deletee = (id) => {
        ApiDelete("/api/messages/" + id).then((data) => { props.delete(); window.location.href = "/messages" });
    };
    return (
        <div >
            <p>
                {props.label} {props.items.length}
            </p>
            <table className="table table-borded">
                <thead>
                    <tr>
                        <th>datum</th>
                        <th>Příjemce</th>
                        <th className='d-none d-sm-block'>text</th>
                        <th colSpan={3}>Akce</th>
                    </tr>
                </thead>
                <tbody>
                    {props.items.map((item, index) => (
                        <tr key={index + 1}>
                            <td>{DateStringFormatter(item.date, true, "numeric")}</td>
                            <td>{item.receiverName}</td>
                            <td className='d-none d-sm-table-cell'>
                                {item.text.slice(0, 10) + "..."}
                            </td>
                            <td>
                                <div className="btn-group">
                                    <Link to={"/messages/show/" + item._id} className="btn btn-sm btn-info">
                                        Zobrazit
                                    </Link>
                                    <Link to={'/messages/edit/' + item._id} className="btn btn-sm btn-warning">
                                        Upravit
                                    </Link>
                                    <button
                                        onClick={deletee.bind(this, item._id)}
                                        className="btn btn-sm btn-danger"
                                    >Odstranit</button>
                                </div>
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
            <div>
                <Link to={"/messages/create/"} className="btn btn-success">Nová zpráva</Link>
            </div>
            <div>
                <br />
            </div>
        </div>

    );
};

export default MessageTable;