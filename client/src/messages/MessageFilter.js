import React from 'react';
import InputSelect from '../common/formFields/InputSelect';
import { InputField } from '../common/formFields/InputField';
const MessageFilter = (props) => {
    const handleChange = (e) => {

        props.handleChange(e);
    };

    const handleSubmit = (e) => {
        props.handleSubmit(e);
    };

    const filter = props.filter;


    return (
        <form onSubmit={handleSubmit}>
            <div className="row">
                <div className="col">
                    <InputSelect
                        name="app"
                        items={props.allowedApps}
                        handleChange={handleChange}
                        label="Aplikace"
                        prompt="nevybrán"
                        value={filter.app}
                    />
                </div>
                <div className="col">
                    <InputField
                        type="text"
                        min="0"
                        name="receiverName"
                        handleChange={handleChange}
                        label="jméno příjemce"
                        prompt="neuveden"
                        value={filter.receiverName}
                    />
                </div>
            </div>

            <div className="row">
                <div className="col">
                    <InputField
                        type="date"
                        min="0"
                        name="fromDate"
                        handleChange={handleChange}
                        label="Od data"
                        prompt="01/01/1990"
                        value={filter.fromDate ? filter.fromYear : ''}
                    />
                </div>

                <div className="col">
                    <InputField
                        type="date"
                        min="0"
                        name="toDate"
                        handleChange={handleChange}
                        label="Do data"
                        prompt="neuvedeno"
                        value={filter.toDate ? filter.toDate : ''}
                    />
                </div>

            </div>

            <div className="row">
                <div className="col">
                    <input
                        type="submit"
                        className="btn btn-secondary float-right mt-2"
                        value={props.confirm}
                    />
                </div>
            </div>
        </form>
    );
};

export default MessageFilter;