import MessageTable from "./MessageTable"
import React, { useEffect, useState } from "react";
import { ApiGet } from "../common/Api"
import MessageFilter from "./MessageFilter";


function MessageIndex(props) {
  const [messageState, setMessages] = useState([]);
  const [allowedAppsState, setAllowedApps] = useState([]);
  const [filterState, setFilter] = useState({
    userId: props.match.params.userId,
    receiverID: undefined,
    fromDate: undefined,
    toDate: undefined,
    limit: undefined,
    curier: undefined
  })
  const deletee = () => {
    ApiGet("/api/messages", null, { userId: localStorage.getItem("userID") }).then((data) => setMessages(data));
  }
  useEffect(() => {
    const params = { userId: props.match.params.userId };
    ApiGet("/api/messages/", null, params).then((data) => setMessages(data));
    ApiGet("/api/allowedApps").then((data) => setAllowedApps(data));
  }, [props.match.params.userId]);
  const handleChange = (e) => {
    if (e.target.value === "false" || e.target.value === "true" || e.target.value === '') {
      setFilter(prevState => {
        return { ...prevState, [e.target.name]: undefined }
      });
    } else {
      setFilter(prevState => {
        return { ...prevState, [e.target.name]: e.target.value }
      });
    }
  }
  const handleSubmit = (e) => {
    e.preventDefault();
    const params = filterState;
    ApiGet('/api/messages/', null, params).then((data) => setMessages(data));
  }
  return (
    <div>
      <h1>Zprávy</h1>
      <hr />
      <MessageFilter
        handleChange={handleChange}
        handleSubmit={handleSubmit}
        allowedApps={allowedAppsState}
        filter={filterState}
        confirm="Filtrovat zprávy"
      />
      <MessageTable delete={deletee} items={messageState} label="Počet zpráv:" />
    </div>
  );
}

export default MessageIndex;