import React, { useState, useEffect } from 'react';
import FlashMessage from '../common/FlalshMessage';
import { ApiGet, ApiPost, ApiPut } from "../common/Api";
import { InputField } from '../common/formFields/InputField';
import { InputSelect } from '../common/formFields/InputSelect';
import { InputCheck } from '../common/formFields/InputCheck';
import { useHistory } from 'react-router-dom';
import DateStringFormatter from '../common/DateStringFormatter';


const MessageForm = (props, hooks) => {

    const [messageIdState, setMessageId] = useState(null);
    const [lonAppState, setLonApp] = useState('');
    const [appState, setApp] = useState('email');
    const [curierIDState, setCurierID] = useState('');
    const [receiverNameState, setReceiverName] = useState('');
    const [receiverIDState, setReceiverID] = useState('');
    const [dateState, setDate] = useState('2002-01-01');
    const [textState, setText] = useState('');
    const [messageSubjectState, setMessageSubject] = useState('');
    const [repeatState, setRepeat] = useState(false);
    const [allowedAppsState, setAllowedApps] = useState([]);
    const [usersAppsState, setUsersApps] = useState([]);
    const [sentState, setSent] = useState(false);
    const [successState, setSuccess] = useState(false);
    const [errorState, setError] = useState();
    const [repeatInDaysState, setRepeatInDays] = useState(0);
    const history = useHistory();
    const userIDState = localStorage.getItem("userID");
    // handleChange(e) {obluha vstupů formuláře}
    const handleChange = (e) => {
        const target = e.target;

        let temp;
        if (target.name === "repeat") {
            temp = target.checked;
        } else {
            temp = target.value;
        }

        const name = target.name;
        const value = temp;

        if (name === 'receiverName') { setReceiverName(value); }
        else if (name === 'receiverID') { setReceiverID(value); }
        else if (name === 'date') { setDate(value); }
        else if (name === 'text') { setText(value); }
        else if (name === 'repeat') { setRepeat(value); if (value !== setRepeatInDays(0)); }
        else if (name === 'subject') { setMessageSubject(value) }
        else if (name === 'repeatInDays') { setRepeatInDays(value) }
        else if (name === 'app') {
            setLonApp(value);
            for (var a = 0; a < usersAppsState.length; a++) {
                const item = usersAppsState[a];
                const longD = item.app + " (" + item.userAppID + ")"
                const isSame = longD === value
                if (isSame) {
                    setCurierID(item._id);
                    setApp(item.app);
                    break;
                }
            }
        };
    };
    // handleSubmit(e) {obsluha odeslání formuláře}
    const handleSubmit = (e) => {
        e.preventDefault();
        const body = {
            curier: curierIDState,
            userID: userIDState,
            receiverName: receiverNameState,
            receiverID: receiverIDState,
            date: dateState,
            subject: messageSubjectState,
            text: textState,
            repeat: repeatState,
            repeatInDays: repeatInDaysState
        };
        (messageIdState
            ? ApiPut('/api/messages/' + props.match.params.id, body)
            : ApiPost('/api/messages/', body)
        )
            .then((data) => {
                setSent(true);
                setSuccess(true);
                history.push('/messages/' + userIDState)

            })
            .catch(async (error) => {
                const er = await error;
                setError(er.message);
                setSent(true);
                setSuccess(false);
            });
    };
    // useEffect() {načtení existujícího záznamu}

    useEffect(() => {
        const id = props.match.params.id || null;
        ApiGet('/api/curiers', null, { userId: localStorage.getItem("userID") }).then((data) => {
            var list = [data.length];
            setLonApp(data[0].app + " (" + data[0].userAppID + ")");
            setApp(data[0].app);
            setCurierID(data[0]._id)
            for (var a = 0; a < data.length; a++) {

                const longApp = data[a].app + " (" + data[a].userAppID + ")";

                list[a] = longApp;
            }
            setUsersApps(data);
            setAllowedApps(list);

        });
        if (id) {
            setMessageId(id);
            ApiGet("/api/messages/" + id).then((data) => {
                setLonApp(data.app.app);
                setReceiverName(data.receiverName);
                setReceiverID(data.receiverID);
                setDate(data.date);
                setText(data.text);
                setMessageSubject(data.subject);
                setCurierID(data.curier);
                if (data.repeatInDays > 0) { setRepeat(true); };
                setRepeatInDays(data.repeatInDays);
                if (data.app === "email") {
                    //setMessageSubject(data.subject);
                }
            });
        } else {
            var today = new Date();
            today = DateStringFormatter(today);
            setDate(today);
        }

    }, [props.match.params.id]);
    // vykreslení formuláře
    const sent = sentState;
    const success = successState;
    var receiverIDLabel = "Zadejte email příjemce:"
    if (lonAppState.app === "messenger") {
        receiverIDLabel = "Zadejte ID příjemce na messengeru"
    }
    return (

        <div>
            <h1>{props.match.params.id ? "Upravit" : "Vytvořit"} zprávu</h1>
            <hr />
            {errorState ? <div className="alert alert-danger">{errorState}</div> : ""}
            {sent && (
                <FlashMessage
                    theme={success ? "success" : ""}
                    text={success ? "Uložení zprávy proběhlo úspěšně." : ""}
                />
            )}

            <form onSubmit={handleSubmit}>
                <InputSelect
                    required={true}
                    name="app"
                    items={allowedAppsState}
                    label="aplikace"
                    prompt="Vyberte aplikaci"
                    value={lonAppState}
                    handleChange={handleChange}
                />

                <InputField
                    required={true}
                    type="text"
                    name="receiverName"
                    label="jméno příjemce"
                    prompt="Zadejte jméno uživatele: "
                    min="0"
                    value={receiverNameState}
                    handleChange={handleChange}
                />

                <InputField
                    required={true}
                    type="text"
                    name="receiverID"
                    label="email příjemce"
                    prompt={receiverIDLabel}
                    min="0"
                    value={receiverIDState}
                    handleChange={handleChange}
                />

                <InputField
                    required={true}
                    type="date"
                    name="date"
                    label="datum odeslání zprávy "
                    prompt="Zadejte datum: "
                    min="0"
                    value={DateStringFormatter(dateState)}
                    handleChange={handleChange}
                />
                {(appState === "email") ? <InputField
                    required={true}
                    type="text"
                    name="subject"
                    label="předmět emailu"
                    prompt="Zadejte předmět emailu "
                    min="0"
                    value={messageSubjectState}
                    handleChange={handleChange}
                />
                    : ""}
                <InputField
                    required={true}
                    type="text"
                    name="text"
                    label="obsah zprávy"
                    prompt="Všechno nejlepší k narozeninám "
                    min="0"
                    value={textState}
                    handleChange={handleChange}
                />

                <InputCheck
                    type="checkbox"
                    name="repeat"
                    label="opakovat"
                    value={repeatState}
                    checked={repeatState}
                    handleChange={handleChange}
                />
                {repeatState ? <div><InputField
                    required={true}
                    type="number"
                    name="repeatInDays"
                    label="opakovat každých __ dní"
                    prompt="zadejte čas ve dnech"
                    min="0"
                    value={repeatInDaysState}
                    handleChange={handleChange}
                /> <div>
                        (Nejbližší datum odeslání zprávy je {DateStringFormatter(dateState, true)})
                    </div>
                </div> : ""}

                <input type="submit" className="btn btn-primary m-2" value="Uložit" />
            </form>
        </div>
    );
}

export default MessageForm;