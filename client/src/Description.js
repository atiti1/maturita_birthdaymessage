
import Collapsible from "./common/Collapsible";
import React from "react";
import { Link } from "react-router-dom";
import register_first from "./images/register_first.png"
import login from "./images/login.png"
import firstLoginSite from "./images/firstLoginSite.png";
import fillInApps from "./images/fillInApps.png";
import listOfApps from "./images/listOfApps.png";
import empty_messages from "./images/empty_messages.png";
import fillAllFields from "./images/fillAllFields.png";
import editButtonsMessage from "./images/editButtonsMessage.png";
import logoutDeleteUser from "./images/logoutDeleteUser.png"
import editMessage from "./images/editMessage.png"

const Description = () => {

    return (
        <div className="container me-2 ms-2 mt-3 mb-5">
            <h1 className="me-2 ms-2">Jak to funguje</h1>
            <div className="mb-3 me-2 ms-2">V programu si nejprve založíte aplikaci, přes kterou chcete zprávy posílat. Pak si vytovříte zprávu (zadáte atributy jako je příjemce,
                datum odeslání, text ...) a pak už jen počkáte až program odešle zprávu za vás. :) Zprávy je možné editovat, mazat, třídit.
            </div>
            <div className="App container">
                <Collapsible
                    false
                    title="Registrace"
                >
                    Účet si založíte tak, že kliknete na tlačítko <Link to={"/register"}>registrovat se </Link>.
                    <div className="text-center w-75 m-2">
                        <img src={register_first} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Při registraci není potřeba d
                    o pole <i>email</i> psát svůj skutečný email - slouží pouze pro účely registrace a přihlašování.
                    Po zaregistrování se přihlašte do programu:
                    <div className="text-center w-75 m-2">
                        <img src={login} className="img-fluid img-thumbnail " alt="" />
                    </div>

                </Collapsible>
                <Collapsible
                    false
                    title="Založení aplikace"
                >
                    Po přihlášení do programu se před vámi objeví toto okno:
                    <div className="text-center w-75 m-2">
                        <img src={firstLoginSite} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Jde zde vidět, že nemáte přidané žádné aplikace ani zprávy.
                    Nejprve je nutné přidat aplikace, protože zprávy jsou na aplikace vázané.
                    Kliknete na pole "Aplikace" a rozbalí se vám okno spravující aplikace:
                    <div className="text-center w-75 m-2">
                        <img src={fillInApps} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Tady kliknete na tlačítko "Přidat aplikaci".
                    Vyplníte všechny údaje. Pro heslo k emailu někteří poksytovalé nabízí možnost hesla pro třetí stranu. (např. Google <a href="">zde</a>).
                    Pokud je to možné, použijte toto heslo. Po vyplnění všech údajů stiskněte "uložit aplikaci".
                    A tím je první aplikace uložena.
                    <div className="text-center w-75 m-2">
                        <img src={listOfApps} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Další aplikace můžete přidávat podle svého uvážení - lze přidat i více emailových účtů apod.
                </Collapsible>
                <Collapsible
                    false
                    title="Vytvoření zprávy"
                >
                    Založení zprávy je podobné jako založení aplikace. Kliknete na "Zprávy" a rozbalí se vám seznam už založených zpráv.
                    <div className="text-center w-75 m-2">
                        <img src={empty_messages} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Pro novou zprávu klikněte na "přidat zprávu". Otevře se vám formulář na přidání nové zprávy:
                    <div className="text-center w-75 m-2">
                        <img src={fillAllFields} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Vyberte si přesjakou aplikaci chcete zprávy
                    posílat, vyplňte všechny údaje a vyberte si, zda chcte zprávu opakovat (a po kolika dnech).
                    Zprávu pak stačí jen uložit a sama se v daný den odešle.

                </Collapsible>
                <Collapsible
                    false
                    title="Změna údajů v aplikaci nebo ve zprávě"
                >   <div className="text-center w-75 m-2">
                        <img src={editButtonsMessage} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Pokud chcete změnit údaje ve zprávě, nebo aplikaci, stačí kliknout na tlačítko upravit u daného předmětu:
                    <div className="text-center w-75 m-2">
                        <img src={editMessage} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Otevře se vám stejný formulářr
                    jako při vytváření nové zprávy a údaje v něm můžete přepsat.
                </Collapsible>
                <Collapsible
                    false
                    title="Smazání aplikace nebo zprávy"
                >
                    <div className="text-center w-75 m-2">
                        <img src={editButtonsMessage} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Pokud aplikaci nechcete už nadále používat, stačí kliknout na tlačítko odebrat. POZOR ! Odstraní se i veškeré zprávy spojené s touto apliakccí.
                    Stejně tak u zprávy - smažete ji stejným způsobem, jako aplikci - přes tlačítko odebrat.
                </Collapsible>
                <Collapsible
                    false
                    title="Odhlášení a smazání uživatele"
                >Pro odhlášení uživatele  stačí kliknout na nápis "Odhlásit se".
                    <div className="text-center w-75 m-2">
                        <img src={logoutDeleteUser} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Pro smazaní účtu uživatele, klikněte na tlačítko "Smazat uživatele". Otevře se vám je
                    ště varování, jestli skutečně chcete smazat živatele, kdteré potvrdíte kliknutím na tlačí
                    tko "Ano". Spolu s uživatelem jsou smazány i všechny zprávy a aplikace, takže od této chvíle se už vám
                    i dříve zadané zprávy neodešlou.
                </Collapsible>
                <Collapsible
                    false
                    title="Co se se zprávami děje po odeslání?"
                >
                    <div className="text-center w-75 m-2">
                        <img src={logoutDeleteUser} className="img-fluid img-thumbnail " alt="" />
                    </div>
                    Zprávy, které se neopakují a byli úspěšné odeslány, se z naší databáze mažou a nemáte k nim tak už přístup.
                    U zpráv, které se mají opakovat, se po odslání změní jejich datum a máte k nim tak pořád přístup.
                    O zprávách, které se nepodařilo doručit, vám dáme vědět na adresu, ze které měly být poslány, společně s chybou, proč se to nepodařilo.
                    Z databáze jsou také vymazány.
                </Collapsible>
            </div>

        </div>
    );
};
export default Description;
