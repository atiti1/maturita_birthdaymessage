/**importování jednotlivých API metod */
const {getItemById, getItemsByCathegories, getUsersApps, getUser} = require("./src/api/Get");
const {postItem, postUser} = require('./src/api/Post');
const {putItem} = require('./src/api/Put');
const {deleteMessage, deleteCurier, deleteUser} = require('./src/api/Delete');
const database = require("./src/Database");

const express = require("express");
const app = express();
const API_PORT = 5000;

app.use(express.json());

/**nastavení CORS na clienta*/
/**const cors = require('cors');
const corsOptions ={
    origin:'https//messageadmin.janousek.me', 
    credentials:true,
    optionSuccessStatus:200
}*/
/**app.use(cors(corsOptions));*/

/**nastavení serveru na port*/
app.listen(API_PORT, () =>
	console.log("Listening on port " + API_PORT + "..."),
);
/**require("./cors")(app);*/

/**GET requests */
/**vrací zprávy daného uživatele v závislosti na vstupních podmínkách */
app.get("/api/messages",(req,res) =>{
	getItemsByCathegories('message',req,res);
});

/**vrací určitou zprávu podle jejího id */
app.get("/api/messages/:id",(req,res) =>{
	getItemById(req.params.id,'message', res);
});

/**vrací všechny kurýry uživatele */
app.get("/api/curiers",(req,res) =>{
	getItemsByCathegories('curier',req,res);
});

/**vrací kurýra podle jeho id */
app.get("/api/curiers/:id",(req,res) =>{
	getItemById(req.params.id,'curier',res);
});

/**vrací přes jaké aplikace umí program posílat zprávy */
app.get("/api/allowedApps",(req,res) =>{
	res.json(database.AllowedCuriers);
});

/**vrací jaké apliakce má daný uživatel nastavené a může přes ně posílat zprávy */
app.get("/api/usersApps",(req,res) =>{
	getUsersApps(req,res);
});

/**vrací, zda existuje uživatel s danými přihlšovacími údaji */
app.get('/api/getUser', (req, res) => {
	getUser(req,res);
  });


/**POST requests */
/**uloží novou zprávy do databáze */
app.post("/api/messages",(req,res)=>{
	postItem(req, 'message',res);
})
/**uloží nového kurýra do databáze */
app.post("/api/curiers",(req,res)=>{
	postItem(req, 'curier',res);
})
/**uloží nového uživatele do databáze */
app.post('/api/addUsers', (req, res) => {
	postUser(req,res);
  });


/**PUT requests */ 
/**změní zprávu i daným id v databázi */
app.put("/api/messages/:id",(req,res)=>{
	putItem(req,'message',res);
})
/**změní kurýra i daným id v databázi */
app.put("/api/curiers/:id",(req,res)=>{
	putItem(req,'curier',res);
})

/**DELETE requsets */ 
/**smaže zprávu s daným id */
app.delete("/api/messages/:id", (req, res) => {
	deleteMessage(req, res);
});
/**smaže kurýra s daným id a všechny zprávy s ním spojené*/
app.delete("/api/curiers/:id", (req, res) => {
	deleteCurier(req, res);
});
/**smaže uživatele s daným id a všechny zprávy a kurýry s ním spojené */
app.delete("/api/users/:id", (req, res) => {
	deleteUser(req, res);
});
// -----------------------------------------------------------------------------
