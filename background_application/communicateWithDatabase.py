"""třída pro komunikaci s databází"""
import pymongo
from bson.objectid import ObjectId
from datetime import datetime, timedelta

import Message


client = pymongo.MongoClient("mongodb://localhost", 27017)
  
database = client.birthdaydb

curier = database["curiers"]
message = database["messages"]

"""vyhledá všechny zprávy daného dne"""
def lookForMessages():
    date = datetime.now()
    date = datetime(date.year,date.month,date.day)
    print(date)
    condition = {"date": date}
    vysledek =message.find(condition)
    return vysledek

"""z jednotlivých zpráv vytvoří pole Message"""
def createListOfMessages():
    vysledek = lookForMessages()
    listOfMessages = []
    for document in vysledek:
        curierCislo = document["curier"]
        cond = {"_id": ObjectId(curierCislo)}
        curierAdd = curier.find_one(cond)
        mess = Message.Message(curierAdd["app"],curierAdd["userAppID"], curierAdd["userAppPassword"], document["receiverID"], document["subject"], document["text"], document["repeatInDays"],document["_id"])
        listOfMessages.append(mess)
    return listOfMessages

"""přepíše datum u zprávy v databázi"""
def rewriteDateOfMessage(mess):
    documentId = mess.id
    cond = {"_id": ObjectId(documentId)}
    newDate = datetime.now() + mess.repeatInDays * 24*3600
    newDate = datetime(newDate.year,newDate.month,newDate.day)
    newValue = {"date":newDate}
    message.update_one(cond,{"$set":newValue})

"""smaže zprávu s daným id"""
def deleteMessage(mess):
    message.delete_one({"_id":mess.id})