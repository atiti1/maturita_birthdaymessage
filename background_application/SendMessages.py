"""hlavní rozcestník pro zpracování zpráv"""
from communicateWithDatabase import createListOfMessages, rewriteDateOfMessage, deleteMessage
from curiers import curierEmail
from curiers import curierJabber

"""zavolá funkci pro vytvoření pole zpráv toho dne
potom tyto zprávy předá giveMessagesToCuriers"""
def akce():
    
    messages = createListOfMessages()
    giveMessagesToCuriers(messages)

"""roztřídí zprávy podle aplikace, ze které mají být poslány
pokud se odeslání nepodaří, zavolá jednu z fukncí sendError na upozornění odesílatele, že se zprávu nepodařilo odeslat"""

def giveMessagesToCuriers(messages):
    sent = 0
    error = 0
    print("Dávám zprávy kurýrům")
    if(len(messages) > 0):
        for document in messages:
            print(document.senderType)
            if(document.senderType == "email"):
                print("posílám to emailCurierovi")
                done = curierEmail.sendMessageEmail(document.senderID, document.senderPassword, document.receiverID, document.subject, document.text)
                if(done["done"] == False):
                    error += 1
                    sendErrorEmail(document.text, document.senderID, done["error"])                    
                else:
                    sent +=1
            elif (document.senderType == "jabber"):
                print("tady by to skočilo do jabberu")
                done = curierEmail.sendMessageEmail(document.senderID, document.senderPassword, document.receiverID, document.text)
                if(done["done"] == False):
                    error += 1
                    sendErrorJabber(document.text, document.senderID, done["error"])   
                else:
                    sent += 1
            if (document.repeatInDays > 0):
                rewriteDateOfMessage(document)
            else:
                deleteMessage(document)
        print("odeslané zprávy", sent)
        print("chybné zprávy", error)
        error=0
        sent=0
    else:
        print("Žádné zadané zprávy")

"""odešle email o neúspěšném pokusu o poslání zprávy (z defaultního emialu na email odesílatele)"""
def sendErrorEmail( text, receiver, err):
    defaultEmail = "project.maturita@gmail.com"
    defaultPassword = "Karota22"
    errMsg = "Dobrý den, \nemail pro adresáta: " + receiver + " (text: " + text + ") se nepodařilo odeslat. Stalo se to kvůli této chybě: " + str(err) + "\nPřipomínač narozenin"
    curierEmail.sendMessageEmail(defaultEmail, defaultPassword, receiver, errMsg)

def sendErrorJabber(text, receiver, err):
    defaultEmail = "projectmaturita@xmpp.jp"
    defaultPassword = "Karota22"
    errMsg = "Dobrý den, \nemail pro adresáta: " + receiver + " (text: " + text + ") se nepodařilo odeslat. Stalo se to kvůli této chybě: " + str(err) + "\nPřipomínač narozenin"
    curierJabber.sendMessageJabber(defaultEmail, defaultPassword, receiver, errMsg)
