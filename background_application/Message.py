"""Třída pro definování zprávy"""

class Message:
    senderType = ""
    senderID = ""
    senderPassword = ""
    receiverID = ""
    subject = ""
    text = ""
    repeatInDays = 0
    id = ""
    def __init__(self, senderType, senderID, senderPassword, receiverID, subject, text, repeatInDays, id):
        self.id = id
        self.senderType = senderType
        self.senderID = senderID
        self.senderPassword = senderPassword
        self.receiverID = receiverID
        self.subject = subject
        self.repeatInDays = repeatInDays
        self.text = text
    
