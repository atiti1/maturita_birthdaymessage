import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

"""vytvoří a odešle email"""
def sendMessageEmail(senderID, senderPassword, receiverID, subject, text):
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = senderID
    msg['To'] = receiverID

    part1 = MIMEText(text, 'plain')
    msg.attach(part1)
    serverUrl = senderID[senderID.index('@') + 1 : ]
    connectUlr = "smtp." + serverUrl
    try:
        with smtplib.SMTP(connectUlr,587) as mail:
            mail.ehlo()
            mail.starttls()
            mail.login(senderID, senderPassword)
            mail.sendmail(senderID,receiverID, msg.as_string())
            mail.quit()
        return {"done": True, "error": ""}
    except Exception as e:
        return {"done": False, "error": e}
    