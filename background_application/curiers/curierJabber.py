import xmpp

"""vytvoří a odešle zprávu přes XMPP"""
def sendMessageJabber(senderID, senderPassword, receiverID, text):
    username = senderID
    passwd = senderPassword
    to= receiverID
    msg= text

    jid = xmpp.protocol.JID(username)
    connection = xmpp.Client(server=jid.getDomain(), debug=[])
    connection.connect()
    connection.auth(user=jid.getNode(), password=passwd, resource=jid.getResource())
    try:
        connection.send(xmpp.protocol.Message(to=to, body=msg))
        return {"done": True, "error": ""}
    except Exception as e :
        return {"done": False, "error": e}
