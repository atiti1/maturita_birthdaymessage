# MESSAGE SENDER
# Úvod
**Message sender** je program na automatické odesílání Jabber zpráv a emailů ([odkaz na ukázkovou instalaci zde](https://messageadmin.janousek.me))\*. Uživatelé si nejprve uloží přes jaký protokol (email/Jabber) chtějí zprávy posílat a zadají svoje přihlašovací údaje. Poté si vytvoří zprávu, kterou chtějí poslat v předem určené datum a program už se postará o správné odeslání v zadaný den. Uživatelé mají ve webovém rozhraní přístup k výpisu aplikací i zpráv , kde je mohou filtrovat, editovat i mazat.


### Využití programu
Program se hodí na rozesílání zpráv, u kterých uživatel ví, že je chce poslat v daný den, ale hrozí, že na ně zapomene. Typické využití tak může být posílání blahopřání k narozeninám a jiným výjimečným událostem. 
Další využití pak může být, že si uživatel nastaví posílání zpráv sám sobě jako upozornění na události, které ho ten den čekají (čas posílání zpráv je 00:00 každý den).

# Ukázková instalace
Ukázková instalace je spuštěná na adrese: [https://messageadmin.janousek.me](https://messageadmin.janousek.me)). Samotný program je kvůli kontrole přístupu a zabezpečení ještě chráněný heslem, pro jehož získání napište na email [zde](kmentova.anna@student.alej.cz). Pak už si ve webovém prostředí můžete vytvořit uživatele a pokračovat dál.


# Ovládání programu

## Registrace
Účet si založíte tak, že kliknete na tlačítko "registrovat se" . 
![alt text](client/src/images/register_first.png)
Při registraci není potřeba do pole "email" psát svůj skutečný email - slouží pouze pro účely registrace a přihlašování. 

Po zaregistrování se přihašte do programu:
![alt text](client/src/images/login.png)

Po přihlášení do programu se před vámi objeví toto okno:
 ![alt text](client/src/images/firstLoginSite.png)

Jde zde vidět, že nemáte přidané žádné aplikace ani zprávy.  
Nejprve je nutné přidat aplikace, protože zprávy jsou na aplikace vázané.

## Založení aplikace
 ![alt text](client/src/images/firstLoginSite.png)

Klikněte na pole "Aplikace" a rozbalí se vám okno spravující aplikace.

 ![alt text](client/src/images/empty_apps.png)

Tady klikněte na tlačítko "Přidat aplikaci":

![alt text](client/src/images/fillInApps.png)

Vyplňte všechny údaje. Pro heslo k emailu někteří poksytovalé nabízí možnost hesla pro třetí stranu. (např. Google [zde](https://myaccount.google.com/apppasswords)). Pokud je to možné, použijte toto heslo. Po vyplnění všech údajů stiskněte talčítko "uložit aplikaci". A tím je první aplikace uložena. Další aplikace můžete přidávat podle svého uvážení - lze přidat i více emailových účtů apod. 

![alt text](client/src/images/listOfApps.png)

## Založení zprávy
Založení zprávy je podobné jako založení aplikace. Kliknete na "Zprávy" a rozbalí se vám seznam už založených zpráv.
![alt text](client/src/images/empty_messages.png)

Pro novou zprávu klikněte na "přidat zprávu". Otevře se vám formulář na přidání nové zprávy. 
![alt text](client/src/images/fillAllFields.png)
Vyberte si přes jakou aplikaci chcete zprávy posílat, vyplňte všechny údaje a vyberte si, zda chcete zprávu opakovat (a po kolika dnech). Zprávu pak stačí jen uložit a sama se v daný den odešle.

## Změna údajů v aplikaci nebo ve zprávě 
![alt text](client/src/images/editButtonsMessage.png)
 Pokud chcete změnit údaje ve zprávě, nebo aplikaci, stačí kliknout na tlačítko upravit u daného předmětu.  
 ![alt text](client/src/images/editMessage.png)
 Otevře se vám stejný formulář jako při vytváření nové zprávy a údaje v něm můžete údaje zprávy přepsat. 
 
 ## Smazání aplikace/zprávy
 ![alt text](client/src/images/editButtonsMessage.png)
 Pokud aplikaci nechcete už nadále používat, stačí kliknout na tlačítko odebrat. POZOR ! Odstraní se i veškeré zprávy spojené s touto apliakací. Stejně tak u zprávy - smažete ji stejným způsobem, jako aplikci - přes tlačítko odebrat.
 
 ## Odhlášení a smazání uživatele
 Pro odhlášení uživatele  stačí kliknout na nápis "Odhlásit se".
 ![alt text](client/src/images/logoutDeleteUser.png)
Pro smazaní účtu uživatele, klikněte na tlačítko "Smazat uživatele". Otevře se vám ještě varování, jestli skutečně chcete smazat živatele, kdteré potvrdíte kliknutím na tlačítko "Ano". Spolu s uživatelem jsou smazány i všechny zprávy a aplikace, takže od této chvíle se už vámi dříve zadané zprávy neodešlou.

 ## Co se děje se zprávami po odeslání? 
  Zprávy, které se neopakují a byli úspěšné odeslány, se z naší databáze mažou a nemáte k nim tak už přístup. U zpráv, které se mají opakovat, se po odslání změní jejich datum a máte k nim tak pořád přístup. O zprávách, které se nepodařilo doručit, vám dáme vědět na adresu, ze které měly být poslány, společně s chybou, proč se to nepodařilo. Z databáze jsou také vymazány.

# Vývojářská dokumentace
Program se skládá ze 3 částí:
* **backround_aplication** - python apilkace, stará se o samotné posílání zpráv
    * javadoc
* **server** - backend, express
    * javadoc
* **client** - frontend, react app
    * javadoc

K ukládání dat používá nerelační databázi [MongoDB](https://www.mongodb.com/try/download/community).

## Instalace
Pro instalaci na svém počítači je nutné je v ním mít nainstalované : 
* [nodejs](https://nodejs.org/en/) + [npm](https://www.npmjs.com/) 
* [python](https://www.python.org/downloads/) + [pip](https://pypi.org/project/pip/) 
* [mongod](https://www.mongodb.com/try/download/community)

Pro další instalaci už je pak možné použít [instalační skript](adresa). Ten pokrývá příkazy pro instalaci frameworků **express, react** (nodejs) a knihoven **concurrently, pymongo, smtplib a xmpp** (python)
ve správných adresářích.
## Spuštění
Spuštění je možné pomocí [spouštěcí skriptu](adresa), nebo pomocí příkazů:
* v kořenovém adresáři: *npm start*
* v adresáři backround_aplication: *python index.js*

## Vývoj
### server
Tato tvoří Backend programu - jedná se o RESTful API server, defaultně poslouchá na adrese *localhost:5000*

systematizace adresářů a souborů:
* **docs** - javadoc
* **node_modules** - moduly nodejs
* **out** - javadoc
* **indx.js** - hlavní kód - definuje jednotlivé API netody - následně volá metody z ./src/api
* **src** - vlastní kód
    * **Database.js** - definuje kolekce v databázi a spravuje spojení s databází
    * **Validation.js** - validuje příchozí requesty
    * **api** - obsehuje metody zajišťující funkčnost API

### client
Tato část tvoří Fronted programu - tedy stará se přihlášení/registraci uživatele, vykreslí seznam zpráv a aplikací a umožňuje jejich správu (vytvoření, editování, mazání) v grafickém prostředí. 

S Backendem komunikuje pomocí **Axiosu**. Defaultně poslouchá na adrese *localhost:4200*

Systematizace adresářů a souborů:
* **public** - složka s veřejnými soubory - obrázky, logo
* **src** - obsahuje zdrojový k aplikaci
    * **App.js** - hlavní třída
    * **apps** - komponenty týkající se vykreslení apliakcí
    * **auth** - komponetny na registraci/přihlášení uživatele
    * **common** - pomocné třídy volané ostatními komponenty
    * **messages** - komponenty týkající se vykreslení zpráv

### backround_aplication
Tato část se stará o samotné odeslání zpráv. K databázi přistupuje přes knihovnu pymnogo. Základní část je SendMessage, která zavolána jendou denně a vypíše zprávy z databáze pro tento den a následně je rozřadí kurýrům pro daný protokol.
Systematizace adresářů a souborů:
* index.js - jednoduchý while cyklus
* SendMessages.js - základní část aplikace
* communicateWithDatabase.js - komunikace s databází
* Message.js - třída pro zprávu
* curiers - jednotlivé skripty pro odesílání zpráv
#
